"""
Имя: Автоматизация актов разногласий по накладной и отмены ТТН
Версия: 1.0
Дата: 22.03.2019
Автор: Гаврилов Андрей
Email: andrey.gavrilov@x5.ru
Tel: 10383
"""

import requests
from xml.etree import ElementTree
from time import sleep, time
from datetime import datetime
from os import system, getcwd, path, mkdir, listdir, remove
from pyzabbix import ZabbixAPI


def create_xml(BASEDIR, template, FSRARID, EGAIS):
    if template == 'RequestRepealWB':
        with open(path.join(BASEDIR,'RequestRepealWB.xml'), 'w') as f:
            f.write("""<?xml version="1.0" encoding="UTF-8"?>
            <ns:Documents Version="1.0"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:ns="http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01"
                xmlns:qp="http://fsrar.ru/WEGAIS/RequestRepealWB">
                <ns:Owner>
                    <ns:FSRAR_ID>""" + FSRARID + """</ns:FSRAR_ID> 
                </ns:Owner>
                <ns:Document>
                    <ns:RequestRepealWB>
                        <qp:ClientId>""" + FSRARID + """</qp:ClientId>
                        <qp:RequestNumber>146757</qp:RequestNumber>
                        <qp:RequestDate>""" + str(datetime.now().date()) + """T10:10:00</qp:RequestDate>
                        <qp:WBRegId>TTN-""" + EGAIS + """</qp:WBRegId>
                    </ns:RequestRepealWB>
                </ns:Document>
            </ns:Documents>
    """)
    elif template == 'WayBillAct_v3':
        with open(path.join(BASEDIR,'WayBillAct_v3.xml'), 'w') as f:
            f.write("""<?xml version="1.0" encoding="UTF-8"?>
                <ns:Documents Version="1.0"
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    xmlns:ns= "http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01"
                    xmlns:wa= "http://fsrar.ru/WEGAIS/ActTTNSingle_v3"
                    xmlns:ce="http://fsrar.ru/WEGAIS/CommonV3">
                    <ns:Owner>
                        <ns:FSRAR_ID>""" + FSRARID + """</ns:FSRAR_ID>
                    </ns:Owner>
                    <ns:Document>
                        <ns:WayBillAct_v3>
                            <wa:Header>
                                <wa:IsAccept>Rejected</wa:IsAccept>
                                <wa:ACTNUMBER>act_rejectedER-0077059</wa:ACTNUMBER>
                                <wa:ActDate>""" + str(datetime.now().date()) + """</wa:ActDate>
                                <wa:WBRegId>TTN-"""+ EGAIS +"""</wa:WBRegId>
                                <wa:Note>111</wa:Note>
                            </wa:Header>
                            <wa:Content></wa:Content>
                        </ns:WayBillAct_v3>
                    </ns:Document>
                </ns:Documents>
            """)



def get_ip(sap):
    z = ZabbixAPI('http://zabbix-head.x5.ru/')
    z.login("store_card_api", "12345")
    find =  z.do_request('host.get', {
        'search': {'name': sap},
        'selectInterfaces': ['ip']
    })['result']
    ip = find[0]['interfaces'][1]['ip'].split('.')[:-1]
    ip = '.'.join(ip) + '.207'
    return ip


def work(BASEDIR, template):
    if not path.exists(path.join(BASEDIR, template + '.csv')):
        with open(path.join(BASEDIR, template + '.csv'), 'w') as f:
            f.write('SAP(номер);FSRAR ID (IN)(12 цифр);TTN EGAIS\n')
        input("Файл " + template + '.csv' +  " не обнаружен и был создан. Заполните.")
        return
    with open(path.join(BASEDIR, template + '.csv'), 'r') as f:
        file = f.readlines()
        header = file[0].rstrip()
        lines = file[1:]
    stores = {}
    for line in lines:
        store = line.split(';')[0]
        FSRARID = line.split(';')[1]
        if len(FSRARID) == 11 and not FSRARID.startswith('0'):
            FSRARID = '0' + FSRARID 
        TTN_EGAIS = line.split(';')[2].rstrip()
        if len(TTN_EGAIS) == 14 and TTN_EGAIS.startswith('TTN-'):
            TTN_EGAIS = TTN_EGAIS[4:]
        if store not in stores:
            stores[store] = {'ip': get_ip(store) , 'timeout': 0, 'done': 0, 'jobs': [{'FSRARID': FSRARID, 'TTN_EGAIS': TTN_EGAIS}]}
        else:
            stores[store]['jobs'].append({'FSRARID': FSRARID, 'TTN_EGAIS': TTN_EGAIS})
    done_count = 0
    date = str(datetime.now()).split('.')[0].replace(' ', '_').replace(':', '-')
    with open( date + '_' + template + '.csv', 'w') as f:
        f.write(header + ';ip;статус отправки\n')
    min_time = 600
    system('cls')
    while done_count != len(lines):
        print(done_count, 'готово из', len(lines), end=' ')
        print('(до следующей отправки', min_time, 'секунд)                      ', end='\r')
        for store in stores:
            if stores[store]['timeout'] == 0 or int(time()) - stores[store]['timeout'] >= 600:
                if stores[store]['done'] < len(stores[store]['jobs']):
                    create_xml(BASEDIR, template, stores[store]['jobs'][stores[store]['done']]['FSRARID'],  stores[store]['jobs'][stores[store]['done']]['TTN_EGAIS'])
                    url = 'http://' + stores[store]['ip'] + ':8195/opt/in/' + template
                    files = {'xml_file': open(path.join(BASEDIR, template + '.xml'), 'rb')}
                    print('Sending xml file to', store, 'UTM with url=', url, end=' ... ')
                    send_response = requests.post(url, files=files)
                    if send_response.status_code == 200:
                        print('Done')
                    else:
                        print('ERROR!')
                    with open(date + '_' + template + '.csv', 'a') as f:
                        f.write(';'.join([store, stores[store]['jobs'][stores[store]['done']]['FSRARID'], stores[store]['jobs'][stores[store]['done']]['TTN_EGAIS'], stores[store]['ip'], str(send_response.status_code)]) + '\n')
                    done_count += 1
                    stores[store]['done'] += 1
                    stores[store]['timeout'] = int(time())
            else:
                if (stores[store]['done'] < len(stores[store]['jobs']) and min_time > 600 - (int(time()) - stores[store]['timeout'])) or min_time <= 1:
                    min_time = 600 - (int(time()) - stores[store]['timeout'])
    input('Все документы отправлены. Коды ответов находятся в отчете.')
    
                

if __name__ == "__main__":
    BASEDIR = getcwd()
    if path.exists(path.join(BASEDIR, 'RequestRepealWB.xml')):
        remove(path.join(BASEDIR, 'RequestRepealWB.xml'))
    if path.exists(path.join(BASEDIR, 'RequestRepealWB.xml')):
        remove(path.join(BASEDIR, 'WayBillAct_v3.xml'))
    while True:
        system('cls')
        print("Выберите действие:\n1. Отправка запроса на отмену акта (RequestRepealWB)\n2. Отмена ТТН (WayBillAct_v3)\n0. Выход")
        key = input('-> ')
        if key == '1':
            work(BASEDIR, 'RequestRepealWB')
        elif key == '2':
            work(BASEDIR, 'WayBillAct_v3')
        elif key == '0':
            break